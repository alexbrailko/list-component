import create from 'zustand';

interface ListsState {
  selectedLists: number[];
  selectList: (lists: number[]) => void;
}

export const useListsStore = create<ListsState>((set) => ({
  selectedLists: [],
  selectList: (lists: number[]) => {
    set(() => ({
      selectedLists: lists,
    }));
  },
}));
