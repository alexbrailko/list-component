export type ListDataType = {
  [key: string]: string | number | undefined;
};

export type RenderInfoType = (value: string, id: number) => JSX.Element;
