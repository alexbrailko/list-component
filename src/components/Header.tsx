import { FC } from 'react';
import { useListsStore } from '../store/lists';

export const Header: FC = () => {
  const selectedLists = useListsStore((state) => state.selectedLists);

  return (
    <header className="App-header">
      <div>Selected items: {selectedLists.toString()}</div>
    </header>
  );
};
