import React, { FC } from 'react';
import { ListDataType, RenderInfoType } from '../types/ListTypes';

type ListInfoProps = {
  data: ListDataType;
  renderInfo: RenderInfoType;
};

export const ListInfo: FC<ListInfoProps> = React.memo(
  ({ data, renderInfo }) => {
    const renderInfoColumn = (obj: ListDataType) => {
      const arr: { id: number; value: string }[] = [];

      Object.entries(obj).forEach(([key, value], index) => {
        if (typeof value === 'string' && key !== 'id') {
          arr.push({ id: index, value });
        }
      });

      return arr.map(({ value, id }) => renderInfo(value, id));
    };

    return <div>{renderInfoColumn(data)}</div>;
  },
);
