import { FC } from 'react';
import List from '@mui/material/List';
import { ListItem } from './ListItem';
import { ListDataType, RenderInfoType } from '../types/ListTypes';

type ListComponentProps = {
  data: ListDataType[];
  renderInfo: RenderInfoType;
};

export const ListComponent: FC<ListComponentProps> = ({ data, renderInfo }) => {
  return (
    <List>
      {data.map((obj, index) => (
        <ListItem
          key={index}
          index={index}
          renderInfo={renderInfo}
          infoData={obj}
        />
      ))}
    </List>
  );
};
