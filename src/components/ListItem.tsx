import { FC } from 'react';
import { default as ListItemMaterial } from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import Checkbox from '@mui/material/Checkbox';
import { useListsStore } from '../store/lists';
import { ListInfo } from './ListInfo';
import { ListDataType, RenderInfoType } from '../types/ListTypes';

type ListItemProps = {
  infoData: ListDataType;
  index: number;
  renderInfo: RenderInfoType;
};

export const ListItem: FC<ListItemProps> = ({
  index,
  renderInfo,
  infoData,
}) => {
  const selectedLists = useListsStore((state) => state.selectedLists);
  const selectList = useListsStore((state) => state.selectList);

  const handleToggle = (value: number) => () => {
    const currentIndex = selectedLists.indexOf(value);
    const newChecked = [...selectedLists];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    selectList(newChecked);
  };

  return (
    <ListItemMaterial
      key={index}
      disablePadding
      sx={{ borderBottom: '1px solid #eee' }}
    >
      <ListItemButton role={undefined} onClick={handleToggle(index)} dense>
        <ListItemIcon>
          <Checkbox
            edge="start"
            checked={selectedLists.indexOf(index) !== -1}
            tabIndex={-1}
            disableRipple
            inputProps={{ 'aria-labelledby': index.toString() }}
          />
        </ListItemIcon>
        <ListInfo data={infoData} renderInfo={renderInfo} />
      </ListItemButton>
    </ListItemMaterial>
  );
};
