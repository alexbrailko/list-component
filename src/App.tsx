import { ListItemText } from '@mui/material';
import { Header } from './components/Header';
import { ListComponent } from './components/ListComponent';

const data = [
  { id: 1, title: 'Title 1' },
  { id: 2, title: 'Title 2' },
  { id: 3, title: 'Title 3' },
];

const data2 = [
  { name: 'Name 1', description: 'Description 1' },
  { name: 'Name 2', description: 'Description 2', link: 'google.com' },
  { name: 'Name 3', description: 'Description 3' },
];

function App() {
  return (
    <div className="App">
      <div className="container">
        <Header />
        <ListComponent
          data={data2}
          renderInfo={(value, id) => <ListItemText key={id} primary={value} />}
        />
      </div>
    </div>
  );
}

export default App;
